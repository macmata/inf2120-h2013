package Main;

/**
* Ce fichier contient l'interface de la classe  TDAMultiEnsemble
* 
* @author ALEXANDRE LEBLANC 
* Code permanent : LEBA03018507 
* Courriel : leblanc.alexandre.6@courrier.uquam.ca
* Cours : INF2120-10
* @version 2013-03-04
*/
import java.util.ArrayList;

public class MultiEnsembleTp1< T> implements TDAMultiEnsemble< T> {

    private ArrayList< T> tab;

    public MultiEnsembleTp1() {
        tab = new ArrayList< T>();
    }

    
    
    /**
     * Retourne true si le multiEnsemble est vide.
     *
     * @return true si ce multiEnsemble est vide, false sinon
     */
    @Override
    public boolean estVide() {
        return tab.isEmpty();
    }

    
    
    /**
     * Vide ce multiEnsemble
     */
    @Override
    public void vider() {
        tab.clear();
    }

    
    
    /**
     * Retourne le nombre d'elements distincts
     *
     * @return le nombre d'elements distincts de ce multiEnsemble
     */
    @Override
    public int cardDistincts() {
        int total = cardTotale();
        ArrayList< T> tabEQte = new ArrayList< T>();
        for (int i = 0; i < total; i++) {
            if (tabEQte.isEmpty()) {
                tabEQte.add(tab.get(i));
            } else {
                if (!(tabEQte.contains(tab.get(i)))) {
                    tabEQte.add(tab.get(i));
                }
            }
        }
        return tabEQte.size();
    }

    
    
    /**
     * return les elements disctincs du multiensemble
     *
     * @return les elements disctinc du multiensemble
     */
    private ArrayList< T> cardDis() {
        int total = cardTotale();
        ArrayList< T> tabEQte = new ArrayList< T>();
        for (int i = 0; i < total; i++) {
            if (tabEQte.isEmpty()) {
                tabEQte.add(tab.get(i));
            } else {
                if (!(tabEQte.contains(tab.get(i)))) {
                    tabEQte.add(tab.get(i));
                }
            }
        }
        return tabEQte;
    }

    
    
    /**
     * Retourne le nombre total d'elements
     *
     * @return le nombre total d'elements incluant les doublons
     */
    @Override
    public int cardTotale() {
        return tab.size();
    }

    
    
    /**
     * Retourne true si ce multiEnsemble contient au moins une occurrence de
     * l'element
     *
     * @param element element a verifier
     * @return true si element est dans le multiEnsemble (au moins une
     * occurrence), false sinon
     */
    @Override
    public boolean estElement(T element) {
        return tab.contains(element);
    }

    
    
    /**
     * Retourne le nombre d'occurrences de l'element dans ce multiEnsemble
     *
     * @param element element a verifier
     * @return le nombre d'occurrences d'element dans ce multiEnsemble
     */
    @Override
    public int nbOccurrences(T element) {
        int nbOfElement = 0;
        for (int i = 0; i < tab.size(); i++) {
            if (element.equals(tab.get(i))) {
                nbOfElement++;
            }

        }
        return nbOfElement;
    }

    
    
    /**
     * Cherche element dans le multiEnsemble.
     *
     * @param element �l�ment � chercher
     * @return une occurrence de l'�l�ment trouv� si au moins une occurrence,
     * null si absent
     */
    @Override
    public T trouverElement(T element) {
        T ret = null;
        if (tab.contains(element)) {
            ret = element;
        }
        return ret;
    }

    
    
    /**
     * Ajoute une occurrence de cet element au multiEnsemble
     *
     * @param element element a ajouter
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES
     * l'ajout
     */
    @Override
    public int ajouter(T element) {
        tab.add(element);
        return nbOccurrences(element);
    }

    
    
    /**
     * Ajoute nbCopies occurrences de cet element au multiEnsemble. nbCopies ne
     * doit pas �tre n�gatif.
     *
     * @param element element a ajouter
     * @param nbCopies nombre d'occurrences a ajouter
     * @throws NombreOccurrencesException si nbCopies est n�gatif.
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES
     * l'ajout
     */
    @Override
    public int ajouter(T element, int nbCopies) {
        int x = 0;
        if (nbCopies < 0) {
            throw new NombreOccurrencesException("Nombre negatif");
        } else {
            while (nbCopies != x) {
                tab.add(element);
                x++;
            }
        }

        return nbOccurrences(element);
    }

    
    
    /**
     * Retire une occurrence de cet element du multiEnsemble. Si l'element etait
     * absent, il le demeure. Ceci veut dire que si l'on retire un element
     * absent et qu'ensuite on l'ajoute, son nombre d'occurrences sera egal a 1.
     *
     * @param element element a retirer
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES le
     * retrait
     */
    @Override
    public int retirer(T element) {
        if (estElement(element)) {
            tab.remove(element);
        }
        return nbOccurrences(element);
    }

    
    
    /**
     * Retire nbCopies occurrences de cet element du multiEnsemble. nbCopies ne
     * doit pas �tre n�gatif.
     *
     * @param element element a retirer
     * @param nbCopies nombre d'occurrences a retirer
     * @throws NombreOccurrencesException si nbCopies est n�gatif.
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES le
     * retrait
     */
    @Override
    public int retirer(T element, int nbCopies) {
        int x = 0;
        if (nbCopies < 0) {
            throw new NombreOccurrencesException("Nombre negatif");
        } else {
            while (x != nbCopies) {
                if (estElement(element)) {
                    tab.remove(element);
                }
                x++;
            }
        }
        return nbOccurrences(element);
    }

    
    
    /**
     * Retire toutes les occurrences de element de ce multiEnsemble. Si
     * l'element etait absent, il le demeure.
     *
     * @param element element a retirer
     * @return le nombre d'occurrences de element dans le multiEnsemble AVANT
     * son �limination
     */
    @Override
    public int eliminer(T element) {
        int qte = nbOccurrences(element);
        for (int i = qte; i >= 0; i--) {
            if (element.equals(tab.get(i))) {
                tab.remove(i);
            }
        }
        return qte;
    }

    
    /**
     * Ajoute � cet ensemble tous les �l�ments de autre. Le multiEnsemble autre
     * sera vid� par l'op�ration. Il s'agit donc d'un transfert des �l�ments de
     * autre dans cet ensemble.
     *
     * @param autre multiEnsemble dont les elements seront transferes dans cet
     * ensemble
     * @return le nombre total d'�l�ments transf�r�s (toutes les occurrences).
     */
    @Override
    public int transfert(TDAMultiEnsemble< T> autre) {
        int xElem = ((MultiEnsembleTp1< T>) autre).cardTotale();
        for (int i = 0; i < xElem; i++) {
            tab.add(((MultiEnsembleTp1< T>) autre).tab.get(i));
        }

        autre.vider();
        return xElem;
    }

    /**
     * redefinition de equals
     * @param o qui est l'objet a compraré
     * @return boolean true ou false
     */
    @Override
    public boolean equals(Object o) {

        return o != null && getClass() == o.getClass() && cardDistincts() == ((MultiEnsembleTp1< T>) o).cardDistincts()
                && cardTotale() == ((MultiEnsembleTp1< T>) o).cardTotale();
    }

    /**
     * permet de retourner une chaine de caractère qui represente le contenue
     * avec lequelle il est utiliser. 
     * redefinition de to toString pour les besoins de
     * l'implementatiion
     * @return une chaine de caractere 
     */
    @Override
    public String toString() {
        String toto = "";
        for (int i = cardDis().size() - 1; i >= 0; i--) {
            toto += (cardDis().get(i) + ", " + nbOccurrences(cardDis().get(i)) + "\n");
        }
        return toto;
    }
}