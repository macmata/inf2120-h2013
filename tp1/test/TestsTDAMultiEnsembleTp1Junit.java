
import Main.NombreOccurrencesException;
import Main.MultiEnsembleTp1;



/**
 * Cette classe permet de tester partiellement la classe MultiEnsembleTp1.
 * 
 * Vous aurez 2 points par test r�ussi pour un total de 42 points
 *
 * @author Louise Laforest 
 * @version 2013-02-11
 */

public class TestsTDAMultiEnsembleTp1Junit extends junit.framework.TestCase {

    MultiEnsembleTp1<String> ensemble;
    MultiEnsembleTp1<String> autreEnsemble;
    
    /**
     * Met en place les engagements.
     * M�thode appel�e avant chaque appel de m�thode de test.
     */
    protected void setUp() {
        ensemble = new MultiEnsembleTp1<String>();
        autreEnsemble = new MultiEnsembleTp1<String>();
    }


    /**
     * Supprime les engagements.
     * M�thode appel�e apr�s chaque appel d'une m�thode de test.
     */
    protected void tearDown() {
        ensemble = null;
        autreEnsemble = null;
    }

    public void verifNombres ( MultiEnsembleTp1<String> ensemble, boolean estVide, int nbDis, int nbTot ) {
        assertEquals ( estVide, ensemble.estVide() );
        assertEquals ( nbDis, ensemble.cardDistincts() );
        assertEquals ( nbTot, ensemble.cardTotale() );
    }

    public void testVide () {
        assertTrue ( ensemble.estVide() );
        assertEquals ( 0, ensemble.cardDistincts() );
        assertEquals ( 0, ensemble.cardTotale() );
    }
    
    public void testVider() {
        ensemble.ajouter ( "A" );
        ensemble.vider();
        verifNombres ( ensemble, true, 0, 0 );
    }
    
    public void testAjouter1 () {
        for ( char c = 'A'; c < 'E'; ++c ) {
            int n = ensemble.ajouter ( "" + c );
            assertEquals ( 1, n );
        }
        verifNombres ( ensemble, false, 4, 4 );
    }

    public void testAjouter2 () {
        int total = 0;
        for ( char c = 'A'; c < 'E'; ++c ) {
            for ( int m = 0; m < c - 'A' + 1; ++m ) {
                int n = ensemble.ajouter ( "" + c );
                ++total;
            }
        }
        verifNombres ( ensemble, false, 4, total );
    }
    
    public void testNbOcc () {
        for ( char c = 'A'; c < 'E'; ++c ) {
            for ( int m = 0; m < c - 'A' + 1; ++m ) {
                int n = ensemble.ajouter ( "" + c );
                assertEquals ( m + 1, n );
            }
        }
        assertEquals ( 1, ensemble.nbOccurrences ( "A" ) );
        assertEquals ( 2, ensemble.nbOccurrences ( "B" ) );
        assertEquals ( 3, ensemble.nbOccurrences ( "C" ) );
        assertEquals ( 4, ensemble.nbOccurrences ( "D" ) );
    }
            
    public void testAjouter4 () {
        try {
            int n = ensemble.ajouter ( "A", -10 );
            fail ( "doit lever NombreOccurrencesException" );
        } catch ( NombreOccurrencesException e ) {
            // test r�ussi
        }
    }

    public void testAjouter5 () {
        int n = ensemble.ajouter ( "A", 0 );
        assertEquals ( 0, n );
        verifNombres ( ensemble, true, 0, 0 );
    }

    public void testEstElementAbsent1 () {
        assertFalse ( ensemble.estElement ( "A" ) );
    }
    
    public void testEstElementAbsent2 () {
        ensemble.ajouter ( "A" );
        assertFalse ( ensemble.estElement ( "B" ) );
    }
    
    public void testEstElementPresent () {
        ensemble.ajouter ( "A" );
        assertTrue ( ensemble.estElement ( "A" ) );
    }
    

    public void testRetirer1 () {
        for ( char c = 'A'; c < 'E'; ++c ) {
            int n = ensemble.retirer ( "" + c );
            assertEquals ( 0, n );
        }
    }

    public void testRetirer2 () {
        for ( char c = 'A'; c < 'E'; ++c ) {
            ensemble.ajouter ( "" + c );
        }
        for ( char c = 'A'; c < 'E'; ++c ) {
            int n = ensemble.retirer ( "" + c, 1 );
            assertEquals ( 0, n );
        }
    }
    

    public void testRetirer3 () {
        for ( char c = 'A'; c < 'E'; ++c ) {
            ensemble.ajouter ( "" + c, 5 );
        }
        for ( char c = 'A'; c < 'E'; ++c ) {
            int n = ensemble.retirer ( "" + c, 7 );
            assertEquals ( 0, n );
        }
    }
        
    public void testEqualsVide1() {
        assertTrue ( ensemble.equals ( autreEnsemble ) );
    }

    public void testEqualsVide2() {
        ensemble.ajouter ( "A" );
        assertFalse ( ensemble.equals ( autreEnsemble ) );
    }

    public void testEqualsVide3() {
        autreEnsemble.ajouter ( "A" );
        assertFalse ( ensemble.equals ( autreEnsemble ) );
    }

    public void testEquals1() {
        ensemble.ajouter ( "A" );
        autreEnsemble.ajouter ( "A" );
        assertTrue ( ensemble.equals ( autreEnsemble ) );
    }

    public void testEquals5() {
        ensemble.ajouter ( "A" );
        autreEnsemble.ajouter ( "A", 3 );
        assertFalse ( autreEnsemble.equals ( ensemble ) );
    }

    public void testEquals6() {
        ensemble.ajouter ( "A" );
        ensemble.ajouter ( "B" );
        ensemble.ajouter ( "C" );
        ensemble.ajouter ( "D" );
        autreEnsemble.ajouter ( "B" );
        autreEnsemble.ajouter ( "D" );
        autreEnsemble.ajouter ( "C" );
        autreEnsemble.ajouter ( "A" );
        assertTrue ( ensemble.equals ( autreEnsemble ) );
        assertTrue ( autreEnsemble.equals ( ensemble ) );
    }

    public void testEqualsNull() {
        assertFalse ( ensemble.equals ( null ) );
    }

    public void testEqualsClasse() {
        assertFalse ( ensemble.equals ( "allo" ) );
    }

}
