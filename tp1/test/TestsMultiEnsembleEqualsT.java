
import Main.MultiEnsembleTp1;



/**
 * Cette classe permet de tester partiellement la classe MultiEnsembleTp1.
 * On teste si la m�thode equals a �t� utilis�e pour tester l'�galit� d'�l�ments
 * 
 * Aucun point n'est allou� pour cette classe
 * 
 * @author Louise Laforest 
 * @version 2013-02-11
 */

public class TestsMultiEnsembleEqualsT {

    public static void main ( String[] args ) {
        
        MultiEnsembleTp1<Integer> multEns = new MultiEnsembleTp1<Integer>();
        
        
        multEns.ajouter ( new Integer ( 5 ) );
        multEns.ajouter ( new Integer ( 8 ), 4 );

        
        System.out.println ( "Nombre d'occurrences de 5 = " + multEns.nbOccurrences ( new Integer ( 5 ) ) + "(1)" );
        System.out.println ( "Nombre d'occurrences de 8 = " + multEns.nbOccurrences ( new Integer ( 8 ) ) + "(4)" );
                

    }
}
