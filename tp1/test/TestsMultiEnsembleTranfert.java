
import Main.MultiEnsembleTp1;


/**
 * Cette classe permet de tester partiellement la classe MultiEnsembleTp1.
 * On teste le transfert
 * 
 * Si ex�cution de cette classe avec votre classe MultiEnsembleTp1 donne tous les bons r�sultats alors
 *     vous avez 5 points
 * sinon
 *     aucun point
 * fin si
 *
 * @author Louise Laforest 
 * @version 2013-02-11
 */
public class TestsMultiEnsembleTranfert {

    public static void main ( String[] args ) {
        
        MultiEnsembleTp1<String> multEns = new MultiEnsembleTp1<String>();
        MultiEnsembleTp1<String> multEns2 = new MultiEnsembleTp1<String>();
        
        int nbTransferts = multEns.transfert ( multEns2 );
        System.out.println ( "Nombre d'�l�ments transf�r�s : " + nbTransferts + "(0)" );
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit �tre vide." );

        multEns.ajouter ( "bonjour" );
        multEns.ajouter ( "bonsoir", 4 );
        multEns.ajouter ( "mamouth", 25 );

        nbTransferts = multEns.transfert ( multEns2 );
        
        System.out.println ( "Nombre d'�l�ments transf�r�s : " + nbTransferts + "(0)" );
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );        
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  bonjour, 1\n  bonsoir, 4\n  mamouth, 25" );
        
        multEns2.ajouter ( "allo", 14 );
        multEns2.ajouter ( "bonsoir" );
        multEns2.ajouter ( "elephant", 22 );

        nbTransferts = multEns.transfert ( multEns2 );
        
        System.out.println ( "Nombre d'�l�ments tranf�r�s : " + nbTransferts + "(37)" );
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );        
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  bonjour, 1\n  bonsoir, 5\n  mamouth, 25\n  allo, 14\n  elephant, 22" );
        
        System.out.println ( "multiEns2.estVide() = " + multEns2.estVide() + "(true)" );
        
    }
}
