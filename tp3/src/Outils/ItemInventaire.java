/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Outils;
import Outils.ItemBase;

/**
 *
 * @author macmata
 */
public class ItemInventaire extends ItemBase{
    private int qte = 0;
    public ItemInventaire ( String desc, double prix, int qte) {
        super(desc,prix);
        this.qte = qte;
    }

    @Override
    public boolean equals(Object o) {
         return o != null && getClass() == o.getClass() && qte ==((ItemInventaire)o).getQte() && getPrix() ==((ItemInventaire)o).getPrix()
                 && getDescription().equals(((ItemInventaire)o).getDescription());                      

    }
    
    
    
    
    /**
     * @return the qte
     */
    public int getQte() {
        return qte;
    }

    /**
     * @param qte the qte to set
     */
    public void setQte(int qte) {
        this.qte = qte;
    }
    
    public String toStringFinal(){
        
        String ret= getQte()+ " "+ getPrix ()+ " " + getDescription();
   
    
    return ret;
    
    }
}
