package Main;

import Outils.ItemInventaire;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.awt.Container;
import java.util.ArrayList;
import javax.swing.*;

public class Tp3 implements ActionListener {

    private ArrayList<ItemInventaire> list;
    private JFrame window;
    private JComboBox comboBox;
    private JButton bouttonAjout;
    private JButton afficher;
    private JButton bouttonSupp;
    private JButton eleminer;
    private JButton nouveau;
    private JButton enregister;
    private JButton afficherConsole;
    private JPanel panel;
    private final int MAX_INT_VALUE = 2147483644;
    private final String ERRER_valInfZero = "Quantite negative ou null";
    private final String ITEM_NOT_SELECTED = "Veuillez selectionner un item.";
    private final String QTE_NOT_VALID = "Quantite non numerique.";
    private final String DESCRIPTION_ = "Description :";
    private final String PRIX_ = "Prix :";
    private final String QUANTITE_ = "Quantite :";
    private final String QTE_TO_ADD = "Quantite a ajouter :";
    private final String QTE_TO_REMOVE = "Quantite a retirer :";

    public Tp3() throws FileNotFoundException, IOException {

        list = scanLineAndExtract();
        window = new JFrame("Items");
        window.setLayout(null);
        window.setBounds(10, 10, 300, 300);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);


        panel = new JPanel(null);
        panel.setBounds(25, 10, 250, 250);
        panel.setBackground(Color.DARK_GRAY);
        panel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        comboBox = new JComboBox();
        bouttonAjout = new JButton("ajouter");
        bouttonSupp = new JButton("Supprimer");
        eleminer = new JButton("Eleminer");
        afficher = new JButton("Afficher");
        nouveau = new JButton("Nouveau");
        enregister = new JButton("Enregistrer");
        afficherConsole = new JButton("Afficher a la console");





        Container container = window.getContentPane();



        eleminer.setBounds(25, 130, 200, 20);
        bouttonSupp.setBounds(25, 30, 200, 20);
        bouttonAjout.setBounds(25, 55, 200, 20);
        afficher.setBounds(25, 80, 200, 20);
        nouveau.setBounds(25, 105, 200, 20);
        comboBox.setBounds(0, 0, 250, 25);
        enregister.setBounds(25, 155, 200, 25);
        afficherConsole.setBounds(25, 185, 200, 25);

        afficher.addActionListener(this);
        eleminer.addActionListener(this);
        bouttonSupp.addActionListener(this);
        bouttonAjout.addActionListener(this);
        comboBox.addActionListener(this);
        nouveau.addActionListener(this);
        enregister.addActionListener(this);
        afficherConsole.addActionListener(this);

        comboBox.addItem(ITEM_NOT_SELECTED);
        for (int i = 0; i < list.size(); ++i) {

            if (list.get(i).getQte() != 0) {

                comboBox.addItem((list.get(i)).getDescription());
            }
        }
        container.add(panel);

        panel.add(afficherConsole);
        panel.add(enregister);
        panel.add(nouveau);
        panel.add(afficher);
        panel.add(bouttonSupp);
        panel.add(bouttonAjout);
        panel.add(eleminer);
        panel.add(comboBox);


        panel.setVisible(true);
        window.setVisible(true);

    }

    public static void main(String[] args) throws FileNotFoundException, IOException {


        Tp3 HelloWolrdProgTp3Final = new Tp3();


    }

    /**
     * toutes les detections de d evenement ce passe ici soit : afficher,
     * bouttonAjout,bouttonSupp,eleminer ,nouveau,enregister,afficherConsole
     *
     * @param evenement
     */
    @Override
    public void actionPerformed(ActionEvent evenement) {

        ItemInventaire x = null;
        JTextField jfiedIn = new JTextField();
        JTextField jtextQte = new JTextField();
        JTextField jtextPrix = new JTextField();
        JTextField jtextDesc = new JTextField();
        Object[] listeEntrees1 = new Object[3];
        Object[] listeEntrees2 = new Object[4];
        Object[] listeEntrees3 = new Object[6];
        
        
        
        /**
         * Bouton Afficher
         */
        if (evenement.getSource() == afficher) {
            if (comboBox.getSelectedItem().toString().equals(ITEM_NOT_SELECTED)) {

                JOptionPane.showMessageDialog(window, ITEM_NOT_SELECTED);

            } else {
                x = getItemFromList(comboBox.getSelectedItem().toString());

                listeEntrees1[0] = DESCRIPTION_ + x.getDescription();
                listeEntrees1[1] = PRIX_ + x.getPrix();
                listeEntrees1[2] = QUANTITE_ + x.getQte();

                JOptionPane.showConfirmDialog(window, listeEntrees1, "Votre item selectionner", JOptionPane.OK_CANCEL_OPTION);
            }
        }
        
        
        
        /**
         * Bouton Ajouter
         */
        if (evenement.getSource() == bouttonAjout) {

            if (comboBox.getSelectedItem().toString().equals(ITEM_NOT_SELECTED)) {

                JOptionPane.showMessageDialog(window, ITEM_NOT_SELECTED);

            } else {

                try {

                    x = getItemFromList(comboBox.getSelectedItem().toString());
                    listeEntrees2[0] = PRIX_ + x.getPrix();
                    listeEntrees2[1] = QUANTITE_ + x.getQte();
                    listeEntrees2[2] = QTE_TO_ADD;
                    listeEntrees2[3] = jfiedIn;

                    int retour = JOptionPane.showConfirmDialog(window, listeEntrees2, QTE_TO_ADD, JOptionPane.OK_CANCEL_OPTION);

                    if (retour == 0 && Integer.parseInt(jfiedIn.getText()) < 0 && Integer.parseInt(jfiedIn.getText()) + x.getQte() < MAX_INT_VALUE) {

                        JOptionPane.showMessageDialog(window, ERRER_valInfZero);

                    } else {
                        if (retour == 0) {
                            setQteItemFromList(comboBox.getSelectedItem().toString(), getItemFromList(comboBox.getSelectedItem().toString()).getQte() + Integer.parseInt(jfiedIn.getText()));
                        }


                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showConfirmDialog(window, null, QTE_NOT_VALID,
                            JOptionPane.OK_OPTION);
                }
            }

        }
        
        
        
        /**
         * Bouton supprimer
         */
        if (evenement.getSource() == bouttonSupp) {
            if (comboBox.getSelectedItem().toString().equals(ITEM_NOT_SELECTED)) {

                JOptionPane.showMessageDialog(window, ITEM_NOT_SELECTED);

            } else {

                try {

                    x = getItemFromList(comboBox.getSelectedItem().toString());
                    listeEntrees2[0] = PRIX_ + x.getPrix();
                    listeEntrees2[1] = QUANTITE_ + x.getQte();
                    listeEntrees2[2] = QTE_TO_REMOVE;
                    listeEntrees2[3] = jfiedIn;

                    int retour = JOptionPane.showConfirmDialog(window, listeEntrees2, QTE_TO_REMOVE,
                            JOptionPane.OK_CANCEL_OPTION);
                    if (retour == 0 && (x.getQte() - Integer.parseInt(jfiedIn.getText())) < 0) {

                        JOptionPane.showMessageDialog(window, ERRER_valInfZero);

                    } else {
                        if (retour == 0) {
                            setQteItemFromList(comboBox.getSelectedItem().toString(), getItemFromList(comboBox.getSelectedItem().toString()).getQte() - Integer.parseInt(jfiedIn.getText()));
                        }
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(window, QTE_NOT_VALID);
                }
            }
        }
        
        
        
        /**
         * Bouton eleminer
         */
        if (evenement.getSource() == eleminer) {
            if (comboBox.getSelectedItem().toString().equals(ITEM_NOT_SELECTED)) {

                JOptionPane.showMessageDialog(window, ITEM_NOT_SELECTED);

            } else {
                int retour = JOptionPane.showConfirmDialog(window, "Vous etes sur le point d'eleminer l'elements", "Eleminer", JOptionPane.OK_CANCEL_OPTION);

                if (retour == 0) {
                    ItemInventaire tobeleminate = getItemFromList(comboBox.getSelectedItem().toString());


                    list.remove(tobeleminate);
                    comboBox.removeItem(comboBox.getSelectedItem());
                }
            }
        }
        
        
        
        /**
         * Bouton Nouveau
         */
        if (evenement.getSource() == nouveau) {


            listeEntrees3[0] = DESCRIPTION_;
            listeEntrees3[1] = jtextDesc;
            listeEntrees3[2] = PRIX_;
            listeEntrees3[3] = jtextPrix;
            listeEntrees3[4] = QUANTITE_;
            listeEntrees3[5] = jtextQte;

            try {
                int retour = JOptionPane.showConfirmDialog(window, listeEntrees3, "Nouvel item",
                        JOptionPane.OK_CANCEL_OPTION);
                if (retour == 0) {

                    if (getItemFromList(jtextDesc.getText()) == null) {
                        if (!jtextDesc.getText().equals(null) && Integer.parseInt(jtextQte.getText()) >= 0 && Double.parseDouble(jtextPrix.getText()) >= 0) {
                            list.add(new ItemInventaire(jtextDesc.getText(), Double.parseDouble(jtextPrix.getText()), Integer.parseInt(jtextQte.getText())));
                            comboBox.addItem(jtextDesc.getText());
                        }
                    } else {
                        JOptionPane.showMessageDialog(window, "Item deja present!");

                    }
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(window, ERRER_valInfZero);
            }


        }
        
        
        
         /**
         * Bouton enregistrer
         */
        if (evenement.getSource() == enregister) {
            try {
                enregistrement(list);
                JOptionPane.showMessageDialog(window, "La sauvegarde est effectuer");
            } catch (IOException ex) {
            }

        }
        if (evenement.getSource() == afficherConsole) {

            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i).toStringFinal());

            }
        }
    }

    /**
     * Permet la creation de la liste a partir du fichier txt/test.txt si le
     * fichier est vide lance les exceptions approprier vous devez lire la doc
     * de la methode extractString pour comrendre la totalite de la methode
     * puisque les 2 travail conjointement
     *
     * @return ArrayList<ItemInventaire>
     * @throws FileNotFoundException
     * @throws IOException
     * @throws NumberFormatException
     */
    public ArrayList<ItemInventaire> scanLineAndExtract() throws FileNotFoundException, IOException, NumberFormatException {
        int TYPE_QTE = 0;
        int TYPE_PRIX = 1;
        int TYPE_DESC = 2;
        ArrayList<ItemInventaire> x = new ArrayList();
        BufferedReader entree;
        String ligneTemp = "";
        entree = new BufferedReader(new FileReader("txt/inventaire.txt"));

        while (ligneTemp != null) {
            ligneTemp = entree.readLine();
            if (ligneTemp != null) {
                x.add(new ItemInventaire(extractString(ligneTemp, TYPE_DESC), Double.parseDouble(extractString(ligneTemp, TYPE_PRIX)), Integer.parseInt(extractString(ligneTemp, TYPE_QTE))));
            }
        }
        entree.close();
        return x;
    }

    /**
     * permet l extraction de la ligne scanner du fichier la methode extrait le
     * type de donne qu on lui demande d extraire soit la quantite , le prix ou
     * la description
     *
     * @param ligneTemp la ligne temporaire scanner du fichier
     * @param type le type dextraction (description, prix, quantite)
     * @return (description ou prix ou quantite)
     */
    public String extractString(String ligneTemp, int type) {
        String x = "";
        String ligne;
        String ret;
        int i = 0;
        int j = 0;
        boolean extracted = false;

        ligne = ligneTemp.trim();

        while (!extracted && i < ligne.length()) {
            while (j != type) {
                if (ligne.charAt(i) != ' ') {

                    i++;
                } else {
                    ligne = ligne.substring(i, ligne.length());
                    ligne = ligne.trim();
                    j++;
                    i = 0;

                }
            }
            if (type != 2) {

                if (ligne.charAt(i) != ' ') {
                    x += ligne.charAt(i);
                    i++;
                } else {
                    extracted = true;
                }
            } else {
                extracted = true;
            }

        }
        if (type == 2) {
            ret = ligne;

        } else {
            ret = x;
        }
        return ret;
    }

    /**
     * retourne une occurence de l item si il est present sinon retourne null
     *
     * @param itemselectionner la desctiption de litem a trouver dans dans la
     * liste
     * @return une occurence de l item
     */
    private ItemInventaire getItemFromList(String itemselectionner) {
        ItemInventaire ret = null;
        for (int i = 0; i < list.size(); ++i) {

            if (list.get(i).getDescription().equals(itemselectionner)) {
                ret = list.get(i);
            }
        }
        return ret;
    }

    /**
     * modifie la quqntite d un item
     *
     * @param itemselectionner
     * @param qte
     */
    private void setQteItemFromList(String itemselectionner, int qte) {

        ItemInventaire x = getItemFromList(itemselectionner);

        if (x != null) {
            x.setQte(qte);
        }
    }

    /**
     * permet l enregistrement du fichier
     *
     * @param liste la liste a enregistrer
     * @throws IOException
     */
    private void enregistrement(ArrayList<ItemInventaire> liste) throws IOException {

        FileWriter fSortie = new FileWriter("txt/inventaire.txt");
        PrintWriter sortie = new PrintWriter(fSortie);

        for (int i = 0; i < liste.size(); i++) {
            sortie.println(liste.get(i).toStringFinal());
        }
        sortie.close();
    }
}
