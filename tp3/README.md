1. Description
Vous devez concevoir une interface graphique permettant de gérer un inventaire. Chaque item de l'inventaire comportera trois
attributs : sa description, son prix unitaire et sa quantité. Tous les items de l'inventaire ont des descriptions différentes. De ce
fait, ce sera cette information qui permettra de chercher un item dans l'inventaire.

2. Fonctionnement du logiciel
Pour démarrer le logiciel, on doit faire exécuter la méthode main de la classe Tp3. Voici le code de cette méthode main :
public static void main ( String[] args ) {
new Tp3();
}
L'interface devra donc être élaborée dans le constructeur de la classe Tp3 (qui devra appeler d'autres méthodes).
Au départ du logiciel, celui-ci doit charger les items de l'inventaire, contenus dans le fichier texte inventaire.txt (dans le
même répertoire que l'application), dans une collection (vous pouvez utiliser MultiEnsembleTp1<ItemInventaire>,
MultiEnsembleTp2<ItemInventaire> ou une ArrayList<ItemInventaire>). Sur chaque ligne de ce fichier se
trouvera la quantité, le prix et la description de l'item. Voici un exemple de ligne :
250
19.99
Mammouth en peluche
Remarquez qu'il peut y avoir aucun, un ou plusieurs espaces en début et en fin de ligne qui doivent être ignorés et un ou
plusieurs espaces entre la quantité et le prix et entre le prix et la description.
Les descriptions (sans le prix et la quantité) des items doivent être placés dans une liste déroulante JComboBox dont le
premier item est "Sélectionnez un item". L'item choisi dans cette liste servira aux opérations 1, 2, 3 et 5.


3. Ce que vous devez remettre
(a) Une classe concrète ItemInventaire qui dérive de la classe abstraite ItemBase (fournie). Elle doit contenir un
attribut pour la quantité en stock. Elle doit aussi contenir au moins un constructeur, un getter et un setter pour l'attribut.
Vous devez respecter le principe d'encapsulation.
(b) La classe Tp3 qui s'occupe de l'interface graphique.
(c) Votre classe MultiEnsembleTp1 ou MultiEnsembleTp2 si vous vous en servez pour stocker l'inventaire.
4. Exigences logicielles
1. Vos classes doivent compiler en Java 6.
2. Vous devez utiliser uniquement les notions GUI vues au cours. Sinon votre travail pourra se voir mériter la note 0.
3.
L'entête de la classe doit être public class Tp3 implements ActionListener. De plus, la méthode
actionPerformed ne doit apparaître qu'une seule fois dans le code.
4. La valeur initialement choisie dans la liste déroulante devra être "Sélectionnez un item".
5. La méthode main devra uniquement créer un objet de la classe Tp3. La création de l'interface graphique se fera
donc dans le constructeur de la classe Tp3.
6. Les différentes opérations devront être déclenchées à l'aide d'un bouton (voir plus loin).
7. Lorsqu'un item est éliminé ou que sa quantité en stock devient 0 suite à un retrait, la description de cet item doit être
       retirée de la liste déroulante et de l'inventaire.
8. Toutes les saisies doivent être validées et les messages d'erreur doivent être affichés dans des boîtes de dialogue
JOptionPane. Validations à faire : caractères non numériques pour un nombre, quantité à ajouter, à retirer, quantité
en stock, prix inférieurs ou égaux à 0.
9. Tous les messages et les saisies doivent se faire dans des boîtes de dialogues de la classe JOptionPane. Pour ce qui
est de la saisie de plusieurs données dans une même boîte pour l'opération Nouveau, veuillez utiliser ce code :
private final int NOMBRE_ENTREES = 3; // description, quantité, prix
private Object[] listeEntrees = new Object[NOMBRE_ENTREES * 2]; // la chaîne et son JTextField
listeEntrees[0] = "Description :";
listeEntrees[1] = variable JTextField correspondant à la description;
listeEntrees[2] = "Prix : ";
listeEntrees[3] = variable JTextField correspondant au prix;
listeEntrees[4] = "Quantité : ";
listeEntrees[5] = variable JTextField correspondant à la quantité;
int retour = JOptionPane.showConfirmDialog ( fenetre, listeEntrees, "Nouvel item",
JOptionPane.OK_CANCEL_OPTION );
Constantes de la classe JOptionPane pour la valeur de retour :
CANCEL_OPTION, OK_OPTION, CLOSED_OPTION
La valeur de retour permet de savoir quel bouton l'utilisateur a cliqué. Si l'utilisateur ne clique sur aucun bouton mais
ferme la fenêtre, c'est équivalent à cliquer « annuler » donc CANCEL_OPTION. (Consultez la documentation de la
classe JOptionPane pour plus de détails.)
10. L'opération Éliminer doit demander à l'utilisateur si c'est bien ce qu'il veut faire (voir les exemples de fenêtres).
11. L'opération Enregister doit écrire dans le fichier inventaire.txt le contenu de l'inventaire. Les informations
doivent être du même format que ce qui est décrit plus haut.
12. (Équipes seulement) L'opération Quitter doit d'abord vérifier s'il y a eu un changement dans l'inventaire (ajout, retrait,
élimination, nouvel item) avant de procéder. Si aucun changement n'est survenu, l'application doit quitter
(fenetre.dispose()). Sinon, elle doit demander si elle doit enregister l'inventaire avant de quitter. Voir les
exemples de fenêtres plus loin pour le choix de l'utilisateur.
