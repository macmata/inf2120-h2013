
import Main.MultiEnsembleTp2;

/**
 * Cette classe permet de tester partiellement la classe MultiEnsembleTp1. On
 * teste si la m�thode equals a �t� utilis�e pour tester l'�galit� d'�l�ments
 *
 * Aucun point n'est allou� pour cette classe
 *
 * @author Louise Laforest
 * @version 2013-02-11
 */
public class TestAlex {

    public static void main(String[] args) {
        MultiEnsembleTp2<String> multEns = new MultiEnsembleTp2<String>();
        multEns.ajouter("bonjour");
        multEns.ajouter("bonsoir", 4);
        multEns.ajouter("mamouth", 25);

        //multEns.printAll();

    }
}
