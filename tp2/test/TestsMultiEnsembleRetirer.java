
import Main.MultiEnsembleTp2;
import Tda.NombreOccurrencesException;





/**
 * Cette classe permet de tester partiellement la classe MultiEnsembleTp1.
 * On teste le retrait et l'�limination d'�l�ments
 * 
 * Si ex�cution de cette classe avec votre classe MultiEnsembleTp1 donne tous les bons r�sultats alors
 *     vous avez 10 points
 * sinon
 *     aucun point
 * fin si
 *
 * @author Louise Laforest 
 * @version 2013-02-11
 */
public class TestsMultiEnsembleRetirer {

    public static void main ( String[] args ) {
        
        MultiEnsembleTp2<String> multEns = new MultiEnsembleTp2<String>();
        
        System.out.println ( "estVide() : " + multEns.estVide() + "(true)" );
        System.out.println ( "cardDistincts() : " + multEns.cardDistincts() + "(0)" );
        System.out.println ( "cardTotale() : " + multEns.cardTotale() + "(0)" );
        System.out.println ( "estElement ( \"allo\" ) : " + multEns.estElement ( "allo" ) + "(false)" );
        
        multEns.ajouter ( "bonjour" );
        multEns.ajouter ( "bonsoir", 4 );
        multEns.ajouter ( "mamouth", 25 );
        multEns.ajouter ( "allo", 14 );
        multEns.ajouter ( "mamouth", 200000000 );
        
        System.out.println("=========================================================");
        
        
        
        
        try {
            multEns.retirer ( "allo", -5 );
            System.out.println ( "*** retirer ( \"allo\", -5 ) ne l�ve aucune exception" );
        } catch ( NombreOccurrencesException e ) {
            System.out.println ( "retirer ( \"allo\", -5 ) l�ve la bonne exception avec ce message : " + e.getMessage() );
        } catch ( Exception e ) {
            System.out.println ( "*** retirer ( \"allo\", -5 ) ne l�ve pas la bonne exception : " + e );
        }
        
        System.out.println ( "estVide() : " + multEns.estVide() + "(false)" );
        System.out.println ( "cardDistincts() : " + multEns.cardDistincts() + "(4)" );
        System.out.println ( "cardTotale() : " + multEns.cardTotale() + "(200000000)" );
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  bonjour, 1\n  bonsoir, 4\n  mamouth, 2600025\n  allo, 14" );
        
        int nombreApres = multEns.retirer ( "bonjour" );
        System.out.println ( "Nombre de \"bonjour\" apr�s le retrait : " + nombreApres + "(0)" );
        nombreApres = multEns.retirer ( "elephant" );
        System.out.println ( "Nombre de \"elephant\" apr�s le retrait : " + nombreApres + "(0)" );
        nombreApres = multEns.retirer ( "bonsoir", 2 );
        System.out.println ( "Nombre de \"bonsoir\" apr�s le retrait : " + nombreApres + "(2)" );
        nombreApres = multEns.retirer ( "allo", 20 );
        System.out.println ( "Nombre de \"allo\" apr�s le retrait : " + nombreApres + "(0)" );
        nombreApres = multEns.retirer ( "mamouth", 0 );
        System.out.println ( "Nombre de \"mamouth\" apr�s le retrait : " + nombreApres + "(2600025)" );
        
        System.out.println("=========================================================");
        
        
        
        
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  bonsoir, 2\n  mamouth, 2600025" );
   
        multEns.ajouter ( "allo", 14 );
        multEns.eliminer ( "bonsoir" );

        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  allo, 14\n  mamouth, 2600025" );
        
        System.out.println ( "\n�limination de mamouth ... (peut prendre plusieurs minutes si elle n'est pas cod�e de fa�on efficace)" );
        // Note du prof : ma version eliminer non optimis�e prend 1005 secondes et celle optimis�e prend 0,092 secondes ...
        // Mac OS 10.7.5, 3.4 GHz Intel Core i7, Java 1.6.0_37, BlueJ 3.0.7
        // Vous aurez 5 points de bonus si la vitesse de la v�tre est proportionnelle � la mienne (optimis�e)
        long tempsAvant;
        long tempsApres;
        double tempsEcoule;
        tempsAvant = System.currentTimeMillis();
        
        multEns.eliminer ( "mamouth" );        
    
        tempsApres = System.currentTimeMillis();
        tempsEcoule = (tempsApres - tempsAvant) / 1000.0;
        System.out.println ( "Temps ecoule : " + tempsEcoule + " secondes." );

        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  allo, 14" );
    }
}
