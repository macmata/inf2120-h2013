import Tda.NombreOccurrencesException;
import Main.MultiEnsembleTp2;



/**
 * Cette classe permet de tester partiellement la classe MultiEnsembleTp1.
 * On teste l'ajout d'�l�ments
 * 
 * Si ex�cution de cette classe avec votre classe MultiEnsembleTp1 donne tous les bons r�sultats alors
 *     vous avez 10 points
 * sinon
 *     aucun point
 * fin si
 * 
 * @author Louise Laforest 
 * @version 2013-02-11
 */

public class TestsMultiEnsembleAjouter {

    public static void main ( String[] args ) {
        
        MultiEnsembleTp2<String> multEns = new MultiEnsembleTp2<String>();
        
        System.out.println ( "estVide() : " + multEns.estVide() + "(true)" );
        System.out.println ( "cardDistincts() : " + multEns.cardDistincts() + "(0)" );
        System.out.println ( "cardTotale() : " + multEns.cardTotale() + "(0)" );
        System.out.println ( "estElement ( \"allo\" ) : " + multEns.estElement ( "allo" ) + "(false)" );
        
        int nombreApres = multEns.ajouter ( "bonjour" );
        System.out.println ( "Nombre de \"bonjour\" apr�s l'ajout : " + nombreApres + "(1)" );
        nombreApres = multEns.ajouter ( "bonsoir", 4 );
        System.out.println ( "Nombre de \"bonsoir\" apr�s l'ajout : " + nombreApres + "(4)" );

        try {
            multEns.ajouter ( "allo", -5 );
            System.out.println ( "*** ajouter ( \"allo\", -5 ) ne l�ve aucune exception" );
        } catch ( NombreOccurrencesException e ) {
            System.out.println ( "ajouter ( \"allo\", -5 ) l�ve la bonne exception avec ce message : " + e.getMessage() );
        } catch ( Exception e ) {
            System.out.println ( "*** ajouter ( \"allo\", -5 ) ne l�ve pas la bonne exception : " + e );
        }

        System.out.println ( "estVide() : " + multEns.estVide() + "(false)" );
        System.out.println ( "cardDistincts() : " + multEns.cardDistincts() + "(2)" );
        System.out.println ( "cardTotale() : " + multEns.cardTotale() + "(5)" );
        System.out.println ( "estElement ( \"allo\" ) : " + multEns.estElement ( "allo" ) + "(false)" );
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  bonjour, 1\n  bonsoir, 4" );
        System.out.println();
        
        String toto = "bon";
        nombreApres = multEns.ajouter ( toto + "jour" ); // permet de v�rifier si equals ou == est utilis� dans MultiEnsembleTp1
        System.out.println ( "Nombre de \"bonjour\" apr�s l'ajout : " + nombreApres + "(2) <-----" );
        nombreApres = multEns.ajouter ( "bonsoir", 4 );
        System.out.println ( "Nombre de \"bonsoir\" apr�s l'ajout : " + nombreApres + "(8)" );
        nombreApres = multEns.ajouter ( "machin", 100 );
        System.out.println ( "Nombre de \"machin\" apr�s l'ajout : " + nombreApres + "(100)" );
        nombreApres = multEns.ajouter ( "truc", 40 );
        System.out.println ( "Nombre de \"truc\" apr�s l'ajout : " + nombreApres + "(40)" );
        nombreApres = multEns.ajouter ( "bidule", 0 );
        System.out.println ( "Nombre de \"bidule\" apr�s l'ajout : " + nombreApres + "(0)" );
        
        System.out.println ( "estVide() : " + multEns.estVide() + "(false)" );
        System.out.println ( "cardDistincts() : " + multEns.cardDistincts() + "(4)" );
        System.out.println ( "cardTotale() : " + multEns.cardTotale() + "(150)" );
        System.out.println ( "estElement ( \"allo\" ) : " + multEns.estElement ( "allo" ) + "(false)" );/// mofid pour alex !!!
        
        System.out.println ( "\nContenu de l'ensemble\n" + multEns );
        System.out.println ( "L'ensemble doit contenir :" );
        System.out.println ( "  bonjour, 2\n  bonsoir, 8\n  machin, 100\n  truc, 40" );
        
    }
}
