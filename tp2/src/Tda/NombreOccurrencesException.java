package Tda;



public class NombreOccurrencesException extends RuntimeException {

    public NombreOccurrencesException ( String message ) {
        super ( message );
    }
}
