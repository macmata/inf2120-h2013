package Tda;

public class Maillon<T> {

    //-----   Variables d'instance -----
    
    private T          info; // donn�e dans le maillon donc le contenue 
    private Maillon<T> suiv; // r�f�rence vers le maillon suivant

    /** Cr�e un nouveau maillon n'ayant pas de maillon suivant
     *  @param o l'information qui sera stock�e dans le mailloelementDebutn
     */
    public Maillon ( T o ) {
        this ( o, null );
    }
    
    /** Cr�e un nouveau maillon ayant un autre maillon existant comme suivant
     *  @param o l'information qui sera stock�e dans le maillon
     *  @param suivant le maillon qui sera le suivant du maillon cr��
     */
    public Maillon ( T o, Maillon<T> suivant ) {
        info = o;
        suiv = suivant;       
    }
    
    //-----   Observateurs (getters) -----
    
    public T info () {
        return info;
    }
    
    public Maillon<T> suivant () {
        return suiv;
    }
    
     
    //-----   Modificateurs (setters) -----
    
    public void modifierInfo ( T o ) {
        info = o;
    }
    
    public void modifierSuivant ( Maillon<T> suivant ) {
        suiv = suivant;
    }

    
} // Maillon