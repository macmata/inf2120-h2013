
/**
* Ce fichier contient l'interface de la classe  TDAMultiEnsemble
* vous y trouver tous les outils necessaire pour l utilisation de  maillons 
* De plus j ai aussi ajouter quelque outils pour debuger qui sont priver mais dont
* vous pouvez changer pour public a des fin de debug.
* 
* CAS LIMITE
* 
* 
* 
* 
* @author ALEXANDRE LEBLANC 
* Code permanent : LEBA03018507 
* Courriel : leblanc.alexandre.6@courrier.uquam.ca
* Cours : INF2120-10
* @version 2013-03-04
*/
package Main;

import Tda.Maillon;
import Tda.TDAMultiEnsemble;
import Tda.NombreOccurrencesException;
import java.util.ArrayList;


public class MultiEnsembleTp2<T> implements TDAMultiEnsemble<T> {
    
    private Maillon<T> tracteur;
    private int nbElements;

    public MultiEnsembleTp2() {
        tracteur = null;
        
     
        
        
    }
     /**
     * Retourne true si le multiEnsemble est vide.
     *
     * @return true si ce multiEnsemble est vide, false sinon
     */
    @Override
    public boolean estVide() {
        return nbElements == 0;
    }
    
    
    
    
    /**
     * Vide ce multiEnsemble
     */
    @Override
    public void vider() {
        nbElements = 0;
        tracteur = null;
    }
    
    
    
    
    /**
     * methode privé qui sert au debugage
     * changer public pour prive une fois le debugage terminer
     * @return 
     */
    private String printAll() {
        Maillon<T> p = tracteur;
        String toto = "fini";
        for (int i = 0; i < nbElements; i++) {
            System.out.println("info: " + p.info() + " pos: " + i);
            p = p.suivant();
        }
        return toto;
    }

    
    
    
    /**
     * Retourne le nombre d'elements distincts
     *
     * @return le nombre d'elements distincts de ce multiEnsemble
     */
    @Override
    public int cardDistincts() {

        ArrayList<T> tab = new ArrayList<T>();
        Maillon<T> p = tracteur;

        while (p != null) {
            if (tab.contains(p.info())) {
                p = p.suivant();

            } else {
                tab.add(p.info());
                p = p.suivant();
            }
        }
        return tab.size();
    }
    
    
    
    
    /**
     * Retourne une chaine contenant les elements distincts
     * Ne pas confondre avec cardDistincts() qui retourne une quantité d
     * 'éléments! ET qui sert au toString
     * @return un ArrayListe contenant tous les éléments distincts
     */
     private ArrayList<T> cardDistinctsListe() {

        ArrayList<T> tab = new ArrayList<T>();
        Maillon<T> p = tracteur;

        while (p != null) {
            if (tab.contains(p.info())) {
                p = p.suivant();
            } else {
                tab.add(p.info());
                p = p.suivant();
            }
        }
        return tab;
    }

     
     
     
    /**
     * Retourne le nombre total d'elements
     *
     * @return le nombre total d'elements incluant les doublons
     */
    @Override
    public int cardTotale() {
        return nbElements;
    }

    
    
    
    /**
     * Retourne le nombre d'occurrences de l'element dans ce multiEnsemble
     *
     * @param element element a verifier
     * @return le nombre d'occurrences d'element dans ce multiEnsemble
     */
    @Override
    public int nbOccurrences(T element) {
        int ret = 0;
        Maillon<T> p = tracteur;

        while (p != null) {

            if (p.info().equals(element)) {

                ret++;

                p = p.suivant();
            } else {

                p = p.suivant();
            }
        }
        return ret;
    }

    
    
    
    /**
     * Retourne true si ce multiEnsemble contient au moins une occurrence de
     * l'element
     *
     * @param element element a verifier
     * @return true si element est dans le multiEnsemble (au moins une
     * occurrence), false sinon
     */
    @Override
    public boolean estElement(T element) {
        boolean ret;
        if (trouverElement(element) == null) {
            ret = false;
        } else {
            ret = true;
        }
        return ret;
    }

    
    
    
    /**
     * Cherche element dans le multiEnsemble.
     *
     * @param element �l�ment � chercher
     * @return une occurrence de l'�l�ment trouv� si au moins une occurrence,
     * null si absent
     */
    @Override
    public T trouverElement(T element) {
        T ret = null;
        Maillon<T> p = tracteur;
        boolean loopFin = true;
        while (p != null && loopFin) {

            if (p.info().equals(element)) {
                ret = p.info();
                loopFin = false;
            } else {

                p = p.suivant();
            }
        }
        return ret;
    }

    

    
    /**
     * cette methode me sert a trouver l'element PUIS le retourne avec sa 
     * position dans la chaine AUTRMENT DIT c'est un pointer qui va ce placer à  
     * l'endroit précis dans la chaine.Ne pas confondre avec trouverElement()
     * qui retourne un boolean
     * @param element l'element dont on recherhe la presence 
     * @return  maillon<T> ,sinon Null
     */
    private Maillon<T> retElement(T element) {
        Maillon<T> ret = null;
        Maillon<T> p = tracteur;
        boolean loopFin = true;
        while (p != null && loopFin) {
            try{
            if (p.suivant().info().equals(element)) {
                ret = p;
                loopFin = false;
            } else {

                p = p.suivant();
            }
             
            }catch(NullPointerException e){
            loopFin = false;
            }
        }
        return ret;
    }

    
    
    
    /**
     * Ajoute une occurrence de cet element au multiEnsemble
     *
     * @param element element a ajouter
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES
     * l'ajout
     */
    @Override
    public int ajouter(T element) {
        
        
            tracteur = new Maillon<T>(element, tracteur);
            nbElements++;// pour eleminer l'tulisation de variable global dans les methodes  
        
        return nbOccurrences(element);
    }

    
    
    /**
     * Ajoute nbCopies occurrences de cet element au multiEnsemble. nbCopies ne
     * doit pas �tre n�gatif.
     *
     * @param element element a ajouter
     * @param nbCopies nombre d'occurrences a ajouter
     * @throws NombreOccurrencesException si nbCopies est n�gatif.
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES
     * l'ajout
     */
    @Override
    public int ajouter(T element, int nbCopies) {


        if (nbCopies < 0) {
            throw new NombreOccurrencesException("Nombre negatif");
        } else {
            for (int i = 0; i < nbCopies; i++) {
                tracteur = new Maillon<T>(element, tracteur);// je n'ai pas utiliser la methode ajouter(T element) mais comme ce n'etait qu'une ligne je l'ai réécrit
                nbElements++;// pour eleminer l'tulisation de variable global dans les methodes
            }
        }
        return nbOccurrences(element);
    }

    

    
    /**
     * Retire une occurrence de cet element du multiEnsemble. Si l'element etait
     * absent, il le demeure. Ceci veut dire que si l'on retire un element
     * absent et qu'ensuite on l'ajoute, son nombre d'occurrences sera egal a 1.
     *
     * @param element element a retirer
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES le
     * retrait
     */
    @Override
    public int retirer(T element) {
        Maillon<T> p = retElement(element);
        
        retraitSansAccesMemoire(element, p);
        return nbOccurrences(element);
    }

    
    
    
    /**
     * Retire nbCopies occurrences de cet element du multiEnsemble. nbCopies ne
     * doit pas �tre n�gatif
     * @param element element a retirer
     * @param nbCopies nombre d'occurrences a retirer
     * @throws NombreOccurrencesException si nbCopies est n�gatif.
     * @return le nombre d'occurrences de element dans le multiEnsemble APRES le
     * retrait
     */
    @Override
    public int retirer(T element, int nbCopies) {
        Maillon<T> p = retElement(element);
        
        if (nbCopies < 0){
         throw new NombreOccurrencesException("Nombre negatif");
        }else{
        for (int i = 0; i < nbCopies; i++) {
            retraitSansAccesMemoire(element, p);

        }
        }
        return nbOccurrences(element);
    }

    
    
    
    /**
     * Retire toutes les occurrences de element de ce multiEnsemble. Si
     * l'element etait absent, il le demeure.
     *
     * @param element element a retirer
     * @return le nombre d'occurrences de element dans le multiEnsemble AVANT
     * son �limination
     */
    @Override
    public int eliminer(T element) {
        int x = nbOccurrences(element);
        retirer(element, x);
        return x;
    }
    
    
    
    
    /*
	 * Ajoute � cet ensemble tous les �l�ments de autre.  Le multiEnsemble autre sera vid� par l'op�ration.
	 * Il s'agit donc d'un transfert des �l�ments de autre dans cet ensemble.
	 * @param autre multiEnsemble dont les elements seront transferes dans cet ensemble
	 * @return le nombre total d'�l�ments transf�r�s (toutes les occurrences).
	 */
    @Override
    public int transfert(TDAMultiEnsemble<T> autre) {
        Maillon <T> q = ((MultiEnsembleTp2<T>)autre).tracteur;
        
      
       int total=((MultiEnsembleTp2<T>)autre).nbElements;
       if (total != 0 ){
           for(int i =0; i<total; i ++){
               
           tracteur = new Maillon<T>(q.info(),q.suivant());
           q = q.suivant();
           nbElements++;
           
           }
           
           ((MultiEnsembleTp2<T>)autre).vider();       
       }    
        return total;
    }
    
    
    
    
    /**
     * redefinition de equals
     * @param o qui est l'objet a compraré
     * @return boolean true ou false
     */
    @Override
    public boolean equals(Object o) {
        return o != null && getClass() == o.getClass() && this == o || (cardDistincts() == ((MultiEnsembleTp2< T>) o).cardDistincts()
                && cardTotale() == ((MultiEnsembleTp2< T>) o).cardTotale());
    }
    
    
    
    
    /**
     * Retourne la cha�ne correspondant au contenu du multiEnsemble. La m�thode
     * toString de T sera utilis�e
     * @return la chaine
     */
    public String toString (){
    String toto = "";
    for (int i =0; i < cardDistinctsListe().size() ; i++) {
    toto += (cardDistinctsListe().get(i) + ", " + nbOccurrences(cardDistinctsListe().get(i)) + "\n");
    }
    return toto;
    
    }
    
    
    
    
    /**
     * Tous le travail de retiré est en fait effectuer ici 
     * pour pouvoir utiliser vous devez dabbord fournir les elements de retiré() 
     * 
     * @param element provient des la methode retiré qui doit etre retiré
     * @param p provient des la methode retiré qui est enfait le maillons deja
     *          trouver dans la chaine ainsi que ca position si il est present 
     */
    private void retraitSansAccesMemoire(T element, Maillon<T> p) {
        Maillon pSuivant;
        try {
            Maillon h = tracteur;
            if (h.info().equals(element)) {
                //System.out.println("h.info().equals(element)" + h.info().equals(element)); pour debug seulement
                tracteur = h.suivant();
                nbElements--;
            } else {
                if(p.suivant().info().equals(element)){// permet une verification. Sans cette ligne mamouth est disfonctionel
                pSuivant = p.suivant().suivant();
                p.suivant().modifierSuivant(null);
                p.modifierSuivant(pSuivant);
                nbElements--;
                
                }
                
            }
        } catch (NullPointerException e) {

           // System.out.println("NullPointerException");//pour debug
        }
    }
}
